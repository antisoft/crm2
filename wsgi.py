#!/usr/bin/env python
"""
    wsgi
    ~~~~

    WSGI module.
"""

from werkzeug.serving import run_simple
from werkzeug.wsgi import DispatcherMiddleware

from crm import frontend, api, admin

application = DispatcherMiddleware(
    frontend.create_app(), {
        '/api': api.create_app(),
        '/admin': admin.create_app(),
    }
)

if __name__ == '__main__':
    run_simple('192.168.111.174', 5000, application, use_reloader=True,
               use_debugger=True)
