# -*- coding: utf-8 -*-
"""
    crm.api.tasks
    ~~~~~~~~~~~~~
"""

from flask import Blueprint, request
import wtforms as wtf
import wtforms.validators as validators

from crm.tools.token import token_required

from crm.api.helpers import *
from crm.models import *
from crm.domain.system.service import *

bp = Blueprint('api_tasks', __name__, url_prefix='/tasks')


def _prepare_task_item(task):
    """
    Format task status

    @list type: crm.domain.tasks.models.task
    """
    task.status = tasks_service.task_statuses[task.status]


class TaskForm(AppForm):
    name = wtf.StringField()
    text = wtf.TextField()
    is_billable = wtf.BooleanField()
    limit = wtf.IntegerField()
    status = wtf.IntegerField()
    project_id = wtf.IntegerField()


@bp.route('/<int:id>')
@token_required
def tasks_get(id):
    task = Task.query.get_or_404(id)
    return response_success(task)


@bp.route('/')
@token_required
def tasks_search():
    # get
    class TasksSearchForm(SearchForm):
        created_by_id = wtf.IntegerField()
        project_id = wtf.IntegerField()
        is_billable = BooleanFieldWithNone()
        name = wtf.TextField()
    form = TasksSearchForm(request.args)
    form.validate_with_exceptions()

    # query
    query = Task.query

    if form.created_by_id.data is not None:
        query = query.filter(Task.created_by_id == form.created_by_id.data)
    if form.project_id.data is not None:
        query = query.filter(Task.project_id == form.project_id.data)
    if form.is_billable.data is not None:
        query = query.filter(Task.is_billable == form.is_billable.data)
    if form.name.data is not None:
        query = query.filter(Task.name.startswith(form.name.data))

    query = form.apply_order(query, {
        'id': Task.id,
        'created_by_id': Task.created_by_id,
        'project_id': Task.project_id,
        'is_billable': Task.is_billable,
        'name': Task.name,
    })

    list = query.offset(form.offset.data).limit(form.limit.data).all()
    return response_success(list, total=query.count())


@bp.route('/', methods=['POST'])
@token_required
def tasks_add():
    current_user = get_current_user()
    form = TaskForm(request.form)
    form.validate_with_exceptions()

    task = Task()
    form.populate_obj(task)
    # task.name = form.name.data
    # task.text = form.text.data
    # task.is_billable = form.is_billable.data
    # task.limit = form.limit.data
    # task.status = form.status.data
    # task.project_id = form.project_id.data
    task.created_by_id = get_current_user_id()

    task.save(db)
    log_info(u'Task {0} added'.format(task.id), current_user.id)
    return response_success(task)


@bp.route('/<id>', methods=['PUT'])
@token_required
def tasks_update(id):
    current_user = get_current_user()
    form = TaskForm(request.form)
    form.validate_with_exceptions()

    dbtask = Task.query.get_or_404(id)
    form.populate_obj(dbtask)
    # dbtask.name = form.name.data
    # dbtask.text = form.text.data
    # dbtask.is_billable = form.is_billable.data
    # dbtask.project_id = form.project_id.data
    # dbtask.status = form.status.data
    # dbtask.limit = form.limit.data

    dbtask.save(db)
    log_info(u'Task {0} updated'.format(dbtask.id), current_user.id)
    return response_success(dbtask)


@bp.route('/<id>', methods=['DELETE'])
@token_required
def tasks_remove(id):
    dbtask = Task.query.get_or_404(id)
    dbtask.status = Task.Status.cancelled
    return response_success(dbtask)
