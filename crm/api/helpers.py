# -*- coding: utf-8 -*-
"""
    crm.api.helpers
    ~~~~~~~~~~~~~~~

    API helpers.
"""

from flask import jsonify
from flask.ext.wtf import Form

import wtforms as wtf
import wtforms.validators as validators

from crm.tools.token import get_token
from crm.helpers import *
from crm.domain.users.models import *


### json responses

def response_success(data, code=0, total=1):
    """Creates json success response"""
    if isinstance(data, (list, tuple)) and total == 1:
        total = len(data)
    code_string = 'S{0:07d}'.format(code)
    return jsonify({
        'success': True,
        'code': code_string,
        'data': data,
        'total': total
    })


def response_error(message, code=0, data=None):
    """Creates json error response"""
    import traceback
    stack_trace = traceback.format_exc()
    code_string = 'E{0:07d}'.format(code)
    return jsonify({
        'success': False,
        'code': code_string,
        'message': message,
        'data': data,
        'stack': stack_trace
    })


### wtf additions

class BooleanFieldWithNone(wtf.BooleanField):
    """WTF boolean field that allows nullable state"""
    def __init__(self, label=None, validators=None, **kwargs):
        super(BooleanFieldWithNone, self).__init__(label, validators, **kwargs)

    def process_data(self, value):
        if value is None:
            self.data = None
        else:
            self.data = bool(value)

    def process_formdata(self, valuelist):
        if not valuelist:
            self.data = None
        elif valuelist[0] in (u'False', u'false', u'f', u'0', u'N', 0, False):
            self.data = False
        elif valuelist[0] in (u'True', u'true', u't', u'1', u'Y', 1, True):
            self.data = True
        else:
            self.data = None


class StringFieldWithNone(wtf.StringField):
    """WTF string field that allows nullable state"""
    def __init__(self, label=None, validators=None, **kwargs):
        super(StringFieldWithNone, self).__init__(label, validators, **kwargs)

    def process_data(self, value):
        if value is None:
            self.data = None
        else:
            self.data = str(value)

    def process_formdata(self, valuelist):
        if not valuelist:
            self.data = None
        else:
            self.data = valuelist[0]


class AppForm(Form):
    """Application specific form"""
    def validate_with_exceptions(self):
        self.csrf_enabled = False
        if not self.validate():
            raise ValidationError(errors=self.errors)


class SearchForm(AppForm):
    offset = wtf.IntegerField(default=0)
    limit = wtf.IntegerField(default=50,
                             validators=[validators.NumberRange(1, 1000)])
    order = wtf.TextField()

    def apply_order(self, query, fields):
        """
        Applies ordering to query. order should be in format "id:desc"
        or "name:asc".
        Fields must be dict of "field name":"class.field". For example:
        { 'id': Job.id, 'name': Job.name }

        @type fields: dict
        """
        order = self.order.data
        if not order or not fields:
            return query
        order = order.lower().strip()
        # order, direction = order.split(':', count=2)
        desc_direction = order.endswith(':desc') > 0
        if order.rfind(':') != -1:
            order = order[0:order.rfind(':')]
        # if ':' in order:
        #     order, direction = ...split
        # else:
        #     direction = 'asc'
        if order in fields:
            field = fields[order]
            if isinstance(field, tuple):
                for _field in field:
                    if desc_direction:
                        query = query.order_by(_field.desc())
                    else:
                        query = query.order_by(_field)
            else:
                if desc_direction:
                    query = query.order_by(field.desc())
                else:
                    query = query.order_by(field)
        return query


### user common mathods

def get_current_user_id():
    """
    @rtype: int
    """
    token = get_token()
    if not token:
        return None
    return int(token.data['uid'])


def get_current_user():
    """
    @rtype: crm.domain.user.models.User
    """
    return User.query.get(get_current_user_id())


def check_user_roles(user, *argv):
    """
    :param user: user to check roles
    """
    roles = [x.role for x in user.roles]
    if User.Role.admin in roles:
        return
    for arg in argv:
        if arg in roles:
            return
    raise ForbiddenError('User must has role ' + ','.join([role.lower() for role in argv]))
