# -*- coding: utf-8 -*-
"""
    crm.api.jobs
    ~~~~~~~~~~~~
"""

from flask import Blueprint, request
import wtforms as wtf

from crm.tools.token import token_required

from crm.api.helpers import *
from crm.models import *


bp = Blueprint('api_jobs', __name__, url_prefix='/jobs')


class JobForm(AppForm):
    text = wtf.StringField()
    is_billable = wtf.BooleanField()
    duration = wtf.IntegerField()
    date = wtf.DateField()
    task_id = wtf.IntegerField()


@bp.route('/<id>')
@token_required
def jobs_get(id):
    job = Job.query.get_or_404(id)
    return response_success(job)


@bp.route('/')
@token_required
def jobs_search():
    # get
    class JobsSearchForm(SearchForm):
        created_by_id = wtf.IntegerField()
        project_id = wtf.IntegerField()
        client_id = wtf.IntegerField()
        task_id = wtf.IntegerField()
        date_from = wtf.DateField()
        date_to = wtf.DateField()
        is_billable = BooleanFieldWithNone()
        text = wtf.TextField()
    form = JobsSearchForm(request.args)
    form.validate_with_exceptions()

    # query
    query = Job.query

    if form.text.data is not None:
        query = query.filter(Job.text.startswith(form.text.data))
    if form.created_by_id.data is not None:
        query = query(Job.created_by_id == form.created_by_id.data)
    if form.date_from.data is not None:
        query = query.filter(Job.date > form.date_from.data)
    if form.date_to.data is not None:
        query = query.filter(Job.date < form.date_to.data)
    if form.is_billable.data is not None:
        query = query.filter(Job.is_billable == form.is_billable.data)

    query = form.apply_order(query, {
        'text': Job.text,
        'created_by_id': Job.created_by_id,
        'date': Job.date,
        'is_billable': Job.is_billable,
    })

    list = query.offset(form.offset.data).limit(form.limit.data).all()
    return response_success(list, total=query.count())


@bp.route('/', methods=['POST'])
@token_required
def jobs_add():
    form = JobForm(request.form)
    form.validate_with_exceptions()

    job = Job()
    form.populate_obj(job)
    job.created_by_id = get_current_user_id()

    job.save(db)
    return response_success(job)


@bp.route('/<id>', methods=['PUT'])
@token_required
def jobs_update(id):
    data = JobForm(request.form)
    data.validate_with_exceptions()

    dbjob = Job.query.get_or_404(id)
    dbjob.text = data.text.data
    dbjob.duration = data.duration.data
    dbjob.date = data.date.data
    dbjob.task_id = data.task_id.data

    dbjob.save(db)
    return response_success(dbjob)


@bp.route('/<id>', methods=['DELETE'])
@token_required
def jobs_remove(id):
    job = Job.query.get_or_404(id)
    job.duration = 0
    job.save(db)
    return response_success(job)
