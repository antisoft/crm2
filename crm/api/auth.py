# -*- coding: utf-8 -*-
"""
    crm.api.auth
    ~~~~~~~~~~~~
"""

from flask import Blueprint, request
import crm.domain.users.service as users_service

from helpers import response_success, response_error
from crm.tools.token import Token, get_token

bp = Blueprint('api_auth', __name__, url_prefix='/auth')


@bp.route('/token', methods=['POST'])
def auth_token():
    user = users_service.authenticate(
        request.form.get('login', ''),
        request.form.get('password', '')
    )
    token = Token({
        'uid': user.id,
        'gid': 0
    })
    return response_success(token.to_dict())


@bp.route('/refresh', methods=['POST'])
def auth_refresh():
    token = get_token()
    if not token:
        return response_error('Invalid token.')
    refresh_token = request.form.get('refresh_token', '')
    if not refresh_token:
        return response_error('Refresh token not specified.')
    if refresh_token != token.refresh_token:
        return response_error('Refresh tokens do not match.')
    token.refresh()
    return response_success(token.to_dict())
