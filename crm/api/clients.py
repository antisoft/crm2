# -*- coding: utf-8 -*-
"""
    crm.api.clients
    ~~~~~~~~~~~~~~~
"""

from flask import Blueprint, request
import wtforms as wtf

from crm.tools.token import token_required

from crm.api.helpers import *
from crm.models import *

bp = Blueprint('api_clients', __name__, url_prefix='/clients')


class ClientForm(AppForm):
    name = wtf.StringField()
    address1 = StringFieldWithNone()
    address2 = StringFieldWithNone()
    city = StringFieldWithNone()
    zip = StringFieldWithNone()
    state = StringFieldWithNone()
    country = StringFieldWithNone()
    sale_user_id = wtf.IntegerField()
    contract_signed = wtf.DateField()
    contract_till = wtf.DateField()
    min_hours_per_month = wtf.IntegerField()
    phone = StringFieldWithNone()
    fax = StringFieldWithNone()
    email = StringFieldWithNone()
    website = StringFieldWithNone()
    billing_email = StringFieldWithNone()
    billing_method = StringFieldWithNone()
    billing_address1 = StringFieldWithNone()
    billing_address2 = StringFieldWithNone()
    billing_city = StringFieldWithNone()
    billing_zip = StringFieldWithNone()
    billing_state = StringFieldWithNone()
    billing_country = StringFieldWithNone()
    note = StringFieldWithNone()


@bp.route('/<id>')
@token_required
def clients_get(id):
    # try:
    #     return response_success(Client.query.get(id))
    # except Client.DoesNotExist:
    #     raise NotFoundError
    query = Client.query
    client = query.filter(Client.id == id).first()
    if client is None:
        raise NotFoundError()
    return response_success(client)


@bp.route('/')
@token_required
def clients_search():
    # get
    class ClientSearchForm(SearchForm):
        name = wtf.StringField()
    form = ClientSearchForm(request.args)
    form.validate_with_exceptions()

    # query
    query = Client.query
    if form.name.data is not None:
        query = Client.query.filter(Client.name.startswith(form.name.data))

    query = form.apply_order(query, {
        'id': Client.id,
        'name': Client.name,
    })

    list = query.offset(form.offset.data).limit(form.limit.data).all()
    return response_success(list, total=query.count())


@bp.route('/', methods=['POST'])
@token_required
def clients_add():
    form = ClientForm(request.form)
    form.validate_with_exceptions()
    client = Client()
    form.populate_obj(client)
    client.save(db)
    return response_success(client)


@bp.route('/<id>', methods=['PUT'])
@token_required
def clients_update(id):
    form = ClientForm(request.form)
    form.validate_with_exceptions()
    dbclient = Client.query.get_or_404(id)
    form.populate_obj(dbclient)
    dbclient.save(db)
    return response_success(dbclient)


@bp.route('/<id>', methods=['DELETE'])
@token_required
def clients_remove(id):
    dbclient = Client.query.get_or_404(id)
    dbclient.removed = datetime.now()
    dbclient.save(db)
    return response_success(dbclient)
