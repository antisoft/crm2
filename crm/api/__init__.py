# -*- coding: utf-8 -*-
"""
    crm.api
    ~~~~~~~

    API package.
"""

from datetime import datetime, date, time

from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException

from flask.sessions import SessionInterface, SessionMixin
from flask.json import JSONEncoder

from crm import factory
from crm.domain import *
from crm.api.helpers import response_error


def create_app(settings_override=None):
    """
    Returns CRM api application instance.
    """
    from crm.tools.validol import ValidolError
    app = factory.create_app(__name__, __path__, settings_override)
    app.json_encoder = ApiJSONEncoder

    for code in default_exceptions.iterkeys():
        app.error_handler_spec[None][code] = (lambda e: (response_error(str(e)), e.code or 500))
    app.errorhandler(ForbiddenError)(lambda e: (response_error(str(e)), 403))
    app.errorhandler(ValueError)(lambda e: (response_error('Conversation error - {0}'.
        format(str(e))), 500))
    app.errorhandler(ValidationError)(lambda e: (response_error(str(e), data=e.errors), 400))
    app.errorhandler(ValidolError)(lambda e: (response_error(str(e)), 400))
    app.errorhandler(HTTPException)(lambda e: (response_error(str(e)), e.code))
    app.errorhandler(BaseException)(lambda e: (response_error(str(e)), 500))
    app.session_interface = NoSessionInterface()
    return app


class ApiJSONEncoder(JSONEncoder):
    """
    Extended json encoder with additional options. It can convert time to
    iso format.
    """
    def default(self, obj):
        if isinstance(obj, JsonSerializable): # hasattr(obj, 'to_json')
            return obj.to_json()
        if isinstance(obj, (datetime, date, time)):
            return obj.isoformat()
        return super(JSONEncoder, self).default(obj)


class Session(dict, SessionMixin):
    """
    Session simplest implementation.
    """
    pass


class NoSessionInterface(SessionInterface):
    """
    This session interface implementation disables sessions at all. For example
    we do not need it for web services.
    """
    session = Session()

    def open_session(self, app, request):
        return self.session

    def save_session(self, app, session, response):
        return
