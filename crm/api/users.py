# -*- coding: utf-8 -*-
"""
    crm.api.users
    ~~~~~~~~~~~~~
"""

from flask import Blueprint, request
import wtforms as wtf
import wtforms.validators as validators

from crm.tools.token import token_required

from crm.api.helpers import *
from crm.models import *
from crm.domain import *
from crm.domain.system.service import *

bp = Blueprint('api_users', __name__, url_prefix='/users')


class UserForm(AppForm):
    first_name = wtf.StringField(validators=[validators.Required()])
    last_name = wtf.StringField(validators=[validators.Required()])
    login = wtf.StringField(validators=[validators.Required()])
    email = wtf.StringField(validators=[validators.Required(), validators.Email()])
    password = wtf.StringField(validators=[validators.Required()])
    status = wtf.IntegerField(default=User.Status.active)
    gender = wtf.StringField(default=User.Gender.unknown)
    phone = wtf.StringField()
    cell = wtf.StringField()
    skype = wtf.StringField()
    birthday = wtf.DateField()


@bp.route('/<int:id>')
@token_required
def users_get(id):
    user = User.query.get_or_404(id)
    user.password = ''
    return response_success(user)


@bp.route('/')
@token_required
def users_search():
    # get
    class UserSearchForm(SearchForm):
        id = wtf.IntegerField()
        name = wtf.StringField()
        status = wtf.IntegerField()
        active = wtf.BooleanField()
    form = UserSearchForm(request.args)
    form.validate_with_exceptions()

    # query
    query = User.query

    if form.id.data is not None:
        query = query.filter(User.id==form.id.data)
    if form.status.data is not None:
        query = query.filter(User.status==form.status.data)
    if form.name.data is not None:
        query = query.filter(User.first_name.startswith(form.name.data))

    query = form.apply_order(query, {
        'name': User.first_name
    })
    list = query.offset(form.offset.data).limit(form.limit.data).all()
    for item in list:
        item.password = ''
    return response_success(list, total=query.count())


@bp.route('/', methods=['POST'])
@token_required
def users_add():
    current_user = get_current_user()
    check_user_roles(current_user, User.Role.human_resource_manager)
    form = UserForm(request.form)
    form.validate_with_exceptions()

    user = User()
    form.populate_obj(user)
    user.created = datetime.now()
    user.set_password(user.password)

    user.save(db)
    log_info(u'User {0} added'.format(user.id), current_user.id)
    return response_success(user)


@bp.route('/<int:id>', methods=['PUT'])
@token_required
def users_update(id):
    current_user = get_current_user()
    check_user_roles(current_user, User.Role.human_resource_manager)
    form = UserForm(request.form)
    form.validate_with_exceptions()

    user = User.query.get_or_404(id)
    form.password.data = user.password
    form.populate_obj(user)
    user.save(db)
    log_info(u'User {0} updated'.format(user.id), current_user.id)
    user.password = ''
    return response_success(user)


@bp.route('/<int:id>', methods=['DELETE'])
@token_required
def users_remove(id):
    current_user = get_current_user()
    check_user_roles(current_user, User.Role.human_resource_manager)
    user = User.query.get_or_404(id)
    user.removed = datetime.now()
    user.save(db)
    log_info(u'User {0} disabled'.format(user.id), current_user.id)
    user.password = ''
    return response_success(user)


@bp.route('/<int:id>/password', methods=['POST'])
@token_required
def users_password_change(id):
    class UserPasswordChangeForm(AppForm):
        current_password = wtf.StringField(validators=[validators.Required()])
        new_password = wtf.StringField(validators=[validators.Required()])
    form = UserPasswordChangeForm(request.form)
    form.validate_with_exceptions()

    user = User.query.get_or_404(id)
    if user.password != form.current_password.data.lower():
        raise ValidationError('Incorrect password')
    user.set_password(form.new_password.data)
    log_info(u'User {0} changed password'.format(user.id), user.id)
    user.save(db)
    return response_success(user)
