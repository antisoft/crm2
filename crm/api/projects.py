# -*- coding: utf-8 -*-
"""
    crm.api.projects
    ~~~~~~~~~~~~~~~~
"""

from flask import Blueprint, request
import wtforms as wtf
import wtforms.validators as validators

from crm.tools.token import token_required

from crm.api.helpers import *
from crm.models import *

bp = Blueprint('api_projects', __name__, url_prefix='/projects')


class ProjectForm(AppForm):
    name = wtf.StringField()
    client_id = wtf.IntegerField()
    is_utilized = wtf.BooleanField()
    is_variable_billing_rate = wtf.BooleanField()
    is_visible_to_client = wtf.BooleanField()
    is_billable = wtf.BooleanField()
    status = wtf.IntegerField()
    start_date = wtf.DateField()
    finish_date = wtf.DateField()
    actual_finish_date = wtf.DateField()
    min_hours_per_month = wtf.IntegerField()
    max_hours_per_month = wtf.IntegerField()
    limit = wtf.IntegerField()
    mailing_list = wtf.StringField()
    billing_type = wtf.StringField()
    billing_frequencies = wtf.StringField()
    hosting_price_per_month = wtf.IntegerField()
    hosting_start_date = wtf.DateField()


@bp.route('/<id>')
@token_required
def projects_get(id):
    job = Project.query.get_or_404(id)
    return response_success(job)


@bp.route('/')
@token_required
def projects_search():
    # get
    class ProjectSearchForm(SearchForm):
        name = wtf.StringField()
        status = wtf.IntegerField()
    form = ProjectSearchForm(request.args)
    form.validate_with_exceptions()

    # query
    query = Project.query

    if form.name.data is not None:
        query = query.filter(Project.name.startswith(form.name.data))
    if form.status.data is not None:
        query = query(Project.status == form.status.data)

    query = form.apply_order(query, {
        'name': Project.text,
        'status': Project.status,
    })

    list = query.offset(form.offset.data).limit(form.limit.data).all()
    return response_success(list, total=query.count())


@bp.route('/', methods=['POST'])
@token_required
def projects_add():
    form = ProjectForm(request.form)
    form.validate_with_exceptions()

    project = Project()
    form.populate_obj(project)
    project.created = datetime.now()

    project.save(db)
    return response_success(project)


@bp.route('/<id>', methods=['PUT'])
@token_required
def projects_update(id):
    form = ProjectForm(request.form)
    form.validate_with_exceptions()

    dbproject = Project.query.get_or_404(id)
    form.populate_obj(dbproject)

    dbproject.save(db)
    return response_success(dbproject)


@bp.route('/<id>', methods=['DELETE'])
@token_required
def projects_remove(id):
    dbproject = Project.query.get_or_404(id)
    dbproject.removed = datetime.now()
    dbproject.save(db)
    return response_success(dbproject)
