# -*- coding: utf-8 -*-
"""
    crm.config
    ~~~~~~~~~~

    Default settings to override. For debug.
"""

DEBUG = True
SQLALCHEMY_DATABASE_URI = 'mysql://ivan:qwevenoma@panel.anti-soft.ru/crm2-dev'
SECRET_KEY = 'this_is_the_development_secret_key_to_override'
JSONIFY_PRETTYPRINT_REGULAR = True
WTF_CSRF_ENABLED = False
