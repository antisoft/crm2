# -*- coding: utf-8 -*-
"""
    crm.frontend
    ~~~~~~~~~~~~

    Launch frontend application.
"""

from crm import factory


def create_app(settings_override=None):
    """
    Returns the Flask frontend instance
    """
    app = factory.create_app(__name__, __path__, settings_override)
    return app
