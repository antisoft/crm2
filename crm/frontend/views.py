# -*- coding: utf-8 -*-
"""
    crm.frontend.views
    ~~~~~~~~~~~~~~~~~~

    Main application view.
"""

from flask import Blueprint

bp = Blueprint('frontend', __name__, static_folder='', static_url_path='')
