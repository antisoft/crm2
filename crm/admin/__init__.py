# -*- coding: utf-8 -*-
"""
    crm.admin
    ~~~~~~~~~

    Administrator area.
"""

from flask.ext.login import LoginManager, current_user
from flask.ext.admin import Admin
from flask.ext.admin.contrib.sqla import ModelView

from crm import factory
from crm.core import db
from crm.domain.users.models import User


def _load_user(userid):
    return User.query.get(userid)


def create_app(settings_override=None):
    """
    Returns the Flask frontend instance
    """
    app = factory.create_app(__name__, __path__, settings_override)
    login_manager = LoginManager()
    login_manager.login_view = 'admin.login'
    login_manager.user_loader(_load_user)
    login_manager.init_app(app)

    # admin
    admin = Admin(app, name='CRM v2', url='/flask-models',
                  endpoint='admin-models', static_url_path='static')
    admin.add_view(AppModelView(User, db.session))
    from crm.domain.tasks.models import Task
    admin.add_view(AppModelView(Task, db.session))
    from crm.domain.projects.models import Project
    admin.add_view(AppModelView(Project, db.session))
    from crm.domain.clients.models import Client
    admin.add_view(AppModelView(Client, db.session))
    from crm.domain.jobs.models import Job
    admin.add_view(AppModelView(Job, db.session))

    return app


class AppModelView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated()
