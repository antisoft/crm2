# -*- coding: utf-8 -*-
"""
    crm.admin.forms
    ~~~~~~~~~~~~~~~
"""

from flask.ext.wtf import Form
import wtforms as wtf
import wtforms.validators as validators


class LoginForm(Form):
    login = wtf.TextField('login', validators=[validators.Required()])
    password = wtf.PasswordField('password', validators=[])


class UserTokenForm(Form):
    login = wtf.TextField('login', validators=[validators.Required()])
    password = wtf.PasswordField('password', validators=[])
    token = wtf.TextField('token')
    user_id = wtf.TextField('user')
    decode = wtf.TextField('decode')


class SystemLogsSearchForm(Form):
    text = wtf.TextField('text')
