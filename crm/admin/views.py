# -*- coding: utf-8 -*-
"""
    crm.admin.views
    ~~~~~~~~~~~~~~~
"""

from flask import (
    render_template, request, Blueprint, redirect, url_for, g)
from flask.ext.login import (login_user, login_required, logout_user,
                             current_user)

from crm.core import db
from crm.domain.users import service
from crm.tools.token import Token, get_token
from crm.domain import ApplicationError
from crm.domain.system.service import *

import forms
from crm.domain.users.models import *
import crm.domain.users.service as user_service

bp = Blueprint('admin', __name__, static_folder='static')


def load_user(userid):
    return User.query.get(userid)


@bp.route('/')
@login_required
def index():
    return render_template('dashboard.html')


@bp.route('/users/token', methods=['GET', 'POST'])
@login_required
def users_token():
    form = forms.UserTokenForm()
    if request.method == 'POST' and form.login.data and form.validate():
        try:
            from hashlib import sha1
            password = sha1(form.password.data).hexdigest().lower()
            user = service.authenticate(form.login.data, password)
            user_token = Token({
                'uid': user.id,
                'gid': 0
            })
            token = user_token.generate()
            form.token.data = token
            form.decode.data = vars(get_token(token))
        except ApplicationError as e:
            form.login.errors.append(str(e))
    if request.method == 'POST' and form.token.data:
        try:
            form.decode.data = vars(get_token(form.token.data))
        except ApplicationError as e:
            form.login.errors.append(str(e))
    return render_template('users_token.html', form=form)


@bp.route('/system/logs', methods=['GET', 'POST'])
@login_required
def system_logs():
    form = forms.SystemLogsSearchForm()
    from crm.domain.system.models import Log
    logs = Log.query.all()
    return render_template('system_logs.html', form=form, logs=logs)


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('admin.index'))
    form = forms.LoginForm()
    if form.validate_on_submit():
        try:
            from hashlib import sha1
            password = sha1(form.password.data).hexdigest().lower()
            user = user_service.authenticate(form.login.data, password)
            # if not user.has_role(User.Role.admin):
            #     logout_user()
            #     raise ValidationError('Only users that have admin role can \
            #                           login here')
            if user is not None:
                log_info('Admin user login {0}'.format(user.full_name()),
                         user.id)
                db.session.commit()
                login_user(user)
                return redirect(request.args.get('next') or
                                url_for('admin.index'))
        except ApplicationError as e:
            form.login.errors.append(str(e))
    return render_template('login.html', form=form)


@bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('admin.login'))


@bp.before_request
def before_request():
    g.user = current_user
