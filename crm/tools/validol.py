# -*- coding: utf-8 -*-
"""
    crm.tools.validol
    ~~~~~~~~~~~~~~~~~
"""


from datetime import datetime
import re


class ValidolError(Exception):
    """Raises when validation exception occurs"""
    def __init__(self, message=None, name=''):
        self.name = name
        self.message = message

    def __str__(self):
        return self.message


class Validol(object):
    """Class validation"""

    def __init__(self, value, name=None):
        self.value = value
        self.name = name
        self.passthrough = False

    ### Tests

    def required(self, message=None):
        if self.passthrough:
            return self
        if self.value is None:
            self._raise_error('required')
        return self

    def bool(self, message=None):
        if self.value is None or self.passthrough:
            return self
        if not isinstance(self.value, bool):
            self._raise_error('must be bool', message)
        return self

    def integer(self, message=None):
        if self.value is None or self.passthrough:
            return self
        if not isinstance(self.value, int):
            self._raise_error('must be integer', message)
        return self

    def float(self, message=None):
        if self.value is None or self.passthrough:
            return self
        if not isinstance(self.value, float):
            self._raise_error('must be float', message)
        return self

    def string(self, message=None):
        if self.value is None or self.passthrough:
            return self
        if not isinstance(self.value, basestring):
            self._raise_error('must be string', message)
        return self

    def datetime(self, message=None):
        if self.value is None or self.passthrough:
            return self
        if not isinstance(self.value, datetime):
            self._raise_error('must be datetime', message)
        return self

    def date(self, message=None):
        from datetime import date
        if self.value is None or self.passthrough:
            return self
        if not isinstance(self.value, date):
            self._raise_error('must be date', message)
        return self

    def min(self, min_value, message=None):
        if self.value is None or self.passthrough:
            return self
        if self.value < min_value:
            self._raise_error('must be greater than {0}'.format(min_value),
                              message)
        return self

    def max(self, max_value, message=None):
        if self.value is None or self.passthrough:
            return self
        if self.value > max_value:
            self._raise_error('must be less than {0}'.format(max_value),
                              message)
        return self

    def range(self, min_value=None, max_value=None, message=None):
        if self.value is None or self.passthrough:
            return self
        if min_value is None or max_value is None:
            return self
        if self.value < min_value or self.value > max_value:
            self._raise_error('must be in range {0} and {1}'.format(
                              min_value, max_value))
        return self

    def len_range(self, min_len=None, max_len=None, message=None):
        if self.value is None or self.passthrough:
            return self
        if self.value is None or self.passthrough:
            return self
        if min_len is None or max_len is None:
            return self
        if len(self.value) < min_len or len(self.value) > max_len:
            self._raise_error('length must be in range {0} and {1}'.format(
                              min_len, max_len))
        return self

    def inside(self, list):
        if self.value is None or self.passthrough:
            return self
        if not self.value in list:
            self._raise_error()
        return self

    def min_len(self, min_len, message=None):
        if self.value is None or self.passthrough:
            return self
        if len(self.value) < min_len:
            self._raise_error('length must be greater than {0}'.format(
                              min_len), message)
        return self

    def max_len(self, max_len, message=None):
        if self.value is None or self.passthrough:
            return self
        if len(self.value) > max_len:
            self._raise_error('length must be less than {0}'.format(
                              max_len), message)
        return self

    def email(self, message=None):
        if self.value is None or self.passthrough:
            return self
        if not re.match('^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$', self.value):
            self._raise_error('must be email', message)
        return self

    def zip(self, message=None):
        if self.value is None or self.passthrough:
            return self
        if not re.match('^\d{5}(-\d{4})?$', self.value):
            self._raise_error('must be zip', message)
        return self

    ### Flow methods

    def when(self, condition):
        self.passthrough = not condition
        return self

    def otherwise(self):
        self.passthrough = not self.passthrough
        return self

    def validate(self):
        return self

    def _raise_error(self, message=None, custom_message=None):
        if message is None:
            message = 'invalid value'
        if custom_message is not None:
            message = custom_message
        if custom_message is None and self.name is not None:
            message = '{0} {1}'.format(self.name, message)
        raise ValidolError(message)


def validate(obj, attr, name=None):
    if not hasattr(obj, attr):
        raise ValidolError('Object does not have attribute %s' % attr)
    if name is None:
        name = attr
    return Validol(getattr(obj, attr), name)
