# -*- coding: utf-8 -*-
"""
    token
    ~~~~~

    Use token for user authentication.

    :copyright (c) 2014 by Ivan Kozhin.
    :license: MIT/X11, see LICENSE for more details.
"""

__author__ = 'Ivan Kozhin'
__license__ = 'MIT/X11'
__copyright__ = '(c) 2014 by Ivan Kozhin'

__all__ = ['TOKEN_NAME', 'TOKEN_EXPIRATION', 'TOKEN_MSG_EXPIRED',
           'TOKEN_MSG_INVALID', 'Token', 'BadToken',
           'ExpiredToken', 'NoTokenDefined', 'get_token',
           'token_required',
           'pre_init_token']

from flask import request, current_app, _request_ctx_stack
from werkzeug.exceptions import Unauthorized
from itsdangerous import URLSafeTimedSerializer
from datetime import timedelta, datetime
from uuid import uuid4
from functools import wraps


### I/O utilities

#: Token name
TOKEN_NAME = 'token'

#: Token default expiration time
TOKEN_EXPIRATION = timedelta(hours=2)

#: Token expired message
TOKEN_MSG_EXPIRED = u'Token expired'

#: Token invalid message
TOKEN_MSG_INVALID = u'Invalid token'

#: Token is not defined message
TOKEN_MGS_NOT_DEFINED = u'Token is not defined'

#: Token cookie domain
TOKEN_COOKIE_DOMAIN = None


# R: reorder
TOKEN_FIELDS = ('created', 'data', 'expires_in', 'refresh_token')


class Token(object):
    """
    This object is used to hold token settings.
    """
    def __init__(self, data=None):
        #: Number of seconds the token expires in. Default value is
        #: TOKEN_EXPIRATION.
        #: After that time user will need to renew key.
        self.expires_in = TOKEN_EXPIRATION.seconds

        #: Token creation time.
        self.created = datetime.now()

        #: The data attached to token.
        self.data = data

        #: Refresh token. Can be used after expiration.
        # R: lazy?
        self.refresh_token = self._generate_refresh_token()

    # @property
    # def refreshed_token(self):
    #     if not hasattr(self, '_refreshed_token'):
    #         self._refreshed_token = self._generate_refresh_token()
    #     return self._refreshed_token

    # @cached_property
    # def refreshed_token(self):
    #     return self._generate_refresh_token()

    def generate(self):
        """
        Generates secured string from token.

        @rtype: str
        """
        token = [
            (self.created-datetime(1970, 1, 1)).total_seconds(),  # 0
            self.data,  # 1
            self.expires_in,  # 2
            self.refresh_token,  # 3
        ]
        token_string = get_signing_serializer().dumps(token)
        return token_string

    def is_expired(self):
        """
        Checks that token is valid (not expired).

        @rtype: bool
        """
        return self.expires_in is not None and \
            self.created+timedelta(seconds=self.expires_in) < datetime.now()

    def refresh(self):
        """
        Refreshes the token. Updates refresh token and created date.
        """
        self.refresh_token = self._generate_refresh_token()
        self.created = datetime.now()

    def to_dict(self):
        """
        Converts to dict.

        @rtype: dict
        """
        # R: duplicate data returned here
        return {
            'expires_in': self.expires_in,
            'refresh_token': self.refresh_token,
            'token': self.generate(),
            'data': self.data,
        }

    def _generate_refresh_token(self):
        return str(uuid4()).replace('-', '')[0:16]


class BadToken(Unauthorized):
    pass


class ExpiredToken(Unauthorized):
    pass


class NoTokenDefined(Unauthorized):
    pass


def get_token(token=None):
    """
    Decodes secured token string to Token. If it is not specified method
    will try to get it from request header or url.

    @type token: str
    @rtype: Token
    """
    # R: ?
    if hasattr(_request_ctx_stack.top, 'token'):
        return _request_ctx_stack.top.token

    # get
    if token is None:
        if 'Authorization' in request.headers:
            token = request.headers['Authorization']
        elif 'token' in request.args:
            token = request.args.get('token')
        elif 'token' in request.cookies:
            token = request.cookies['token']

    if token is None:
        return None

    # decrypt
    signer = get_signing_serializer()
    # R: _, _, token_str, expires, refresh_token = singner.loads(...)
    token_array = signer.loads(token)

    # parse
    # R: token serialize/deserialize logic should be kept together
    token = Token(token_array[1])
    token.expires_in = token_array[2]
    token.refresh_token = token_array[3]
    token.created = timedelta(seconds=token_array[0])+datetime(1970, 1, 1)

    # check
    if token.is_expired():
        raise ExpiredToken()

    # R: cache set and get should be done in single level
    pre_init_token(token)
    return token


def get_signing_serializer():
    if not current_app.secret_key:
        return None
    kwargs = {
        'key_derivation': 'hmac'
    }
    #: make salt more unique
    return URLSafeTimedSerializer(current_app.secret_key,
                                  salt='token', serializer=None,
                                  signer_kwargs=kwargs)


def token_required(func):
    """
    If you decorate view of this it will ensure that request has correct
    token. The decorate will be excluded in testing mode.

    @param func: the view function to decorate
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_app.config.get('TESTING', False):
            return func(*args, **kwargs)
        token = get_token()
        if token is None:
            raise NoTokenDefined()
        return func(*args, **kwargs)
    return decorated_view


def pre_init_token(token):
    """
    The will pre init request with token. Useful for testing.

    @param token: Token
    """
    _request_ctx_stack.top.token = token
