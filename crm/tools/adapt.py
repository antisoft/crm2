# -*- coding: utf-8 -*-
"""
    crm.tools.adapt
    ~~~~~~~~~~~~~~~

    Adapt your data to specific format and defaults.
"""


class Adapt:
    """
    Adapt data to format you want.
    """
    value = None

    def __init__(self, value):
        self.value = value

    def none_safe(self):
        if self.value is None:
            self.value = u''
        return self

    def default(self, value):
        if self.value is None:
            self.value = value
        return self

    def default_or_empty(self, value):
        if self.value is None or len(self.value) == 0:
            self.value = value
        return self

    def integer(self):
        self.value = int(self.value)
        return self

    def get(self):
        return self.value


def adapt(value):
    return Adapt(value)
