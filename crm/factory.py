# -*- coding: utf-8 -*-
"""
    crm.factory
    ~~~~~~~~~~~

    Factory module.
"""

from flask import Flask

from helpers import register_blueprints
from core import db


def create_app(import_name, package_path, settings_override=None):
    """
    Returns an instance of Flask application.

    :param import_name: application package name
    :param package_path: application package path
    :param settings_override: a dictionary of settings to override
    """
    app = Flask(import_name, instance_relative_config=True)

    app.config.from_object('crm.config')
    app.config.from_pyfile('crm.config', silent=True)
    app.config.from_object(settings_override)

    db.init_app(app)

    register_blueprints(app, import_name, package_path)

    return app
