# -*- coding: utf-8 -*-
"""
    crm.manage.database
    ~~~~~~~~~~~~~~~~~~~

    Database management commands.
"""

import datetime

from flask.ext.script import Command, Option
from sqlalchemy import create_engine

from crm.core import db
from crm.models import *

import oldcrm


class CreateDatabaseCommand(Command):
    """
    Creates database. Can be test or migrate from old.
    """
    option_list = (
        Option('--testdata', '-t', dest='testdata', default=False,
               action="store_true"),
        Option('--migrate', '-m', dest='migrate'),
    )
    logfilename = './tmp/manage-{0}.log'.format(datetime.now().
                  strftime('%Y%m%d-%H%M%S'))

    def log(self, text):
        logfile = open(self.logfilename, 'a')
        logfile.write((text+'\n').encode('utf8'))
        logfile.close()
        print(text)

    def run(self, migrate, testdata):
        try:
            db.drop_all()
            db.create_all()
            self.log('Recreated database')
            if migrate:
                self.migrate_from_old_database(migrate)
                self.log('Migration done from old CRM database')
            elif testdata:
                self.fill_test_data()
                self.log('Updated database with test data')
        except Exception as e:
            import traceback
            traceback.print_exc()
            self.log('[ERROR] {0}'.format(e))
            raise e

    def fill_test_data(self):
        """
        Prefill new database with test data.
        """

        # test departments
        department = Department(name='undefined')
        department.validate()
        db.session.add(department)  # 1

        # test users
        user = User()
        user.first_name = u'Ivan'
        user.last_name = u'Kozhin'
        user.login = 'ivan'
        user.email = 'ivan@saritasa.com'
        user.password = '40bd001563085fc35165329ea1ff5c5ecbdbbeef'
        user.adapt()
        user.validate()
        db.session.add(user)

        # test clients
        client = Client()
        client.name = 'KillBill Corp.'
        client.adapt()
        client.validate()
        db.session.add(client)

        db.session.commit()

    def migrate_from_old_database(self, connection_string):
        """
        Copy old data from old mssql database to new one.
        """
        engine = create_engine(connection_string)
        connection = engine.connect()

        self._import_users(connection)
        self._import_clients(connection)
        self._import_projects(connection)
        self._import_tasks(connection)
        self._import_jobs(connection)

        connection.close()

    def _format_phone(self, phone):
        if phone and len(phone.strip()) > 3 and phone.find('_') == -1:
            return phone
        else:
            return None

    def _format_state(self, state):
        if state and state.lower().strip() == 'california':
            return 'CA'
        if state and state.lower().strip() == 'new york':
            return 'NY'
        if state and state.lower().strip() == 'pennsylvan':
            return 'PA'
        if state and state.lower().strip() == 'ural':
            return None
        if state and state.lower().strip() == 'florida':
            return 'FL'
        if state and state.lower().strip() == 'new jersey':
            return 'NJ'
        if state and state.lower().strip() == 'vermont':
            return 'VT'
        return state

    def _format_country(self, country):
        if country and country.lower().strip() == 'russia':
            return 'RUS'
        if country and country.lower().strip() == 'uk':
            return 'GBR'
        if country and country.lower().strip() == 'canada':
            return 'CAN'
        return country

    def _format_text(self, text, format_title=True, put_dot=False,
                     remove_dot=False):
        if text is None:
            return ''
        if format_title and len(text) > 1:
            text = text[0].title() + text[1:]
        text = text.replace('  ', ' ').replace('   ', ' '). \
            replace('    ', ' ').replace('     ', ' ')
        if put_dot and text.find('.') != len(text)-1:
            text += '.'
        if remove_dot and text.find('.') == len(text)-1:
            text = text[:len(text)]
        return text

    def _import_users(self, connection):
        """
        Import users.
        """
        result = connection.execute(oldcrm.QUERY_USERS)
        count = 0
        for row in result:
            status = USER_STATUS_ACTIVE
            if row['generalStatusID'] == 30:
                status = USER_STATUS_FIRED
            if not row['email'] or len(row['email']) < 5:
                pass
                #self.log('[U] Skipped user {0} because of bad email'.format(
                #         row['userID']))
                #continue
            if row['typeID'] == 'C ' and row['userID'] in (
                126, 168, 133, 166, 167, 186, 189, 224, 225, 229, 235, 252,
                254, 255, 267, 278, 335, 357, 360, 386, 392, 395, 432, 437,
                461, 462, 522, 525, 552, 566, 569, 570, 572, 739, 768, 778
            ):
                self.log('[U] Skipped user {0} because in bad list'.format(
                         row['userID']))
                continue
            user = User()
            user.id = row['userID']
            user.first_name = self._format_text(row['firstName'].decode('cp1251'))
            user.last_name = self._format_text(row['lastName'].decode('cp1251'))
            user.birthday = row['birthday']
            user.login = row['login']
            if user.id == 458:
                user.login = 'bidus@yar-do.ru'
            user.password = row['password']
            if not row['email'] or len(row['email']) < 5:
                user.email = 'none@example.com'
            else:
                user.email = row['email']
            if row['skype'] and len(row['skype']) > 3:
                user.skype = row['skype']
            user.phone = self._format_phone(row['phone'])
            user.created = row['created']
            user.updated = row['updated']
            user.removed = row['deleted']
            user.cell = self._format_phone(row['cell'])
            user.status = status
            if row['typeID'] == 'C ':
                user.type = USER_TYPE_CLIENT
                if not user.login or len(user.login) > 40:
                    user.login = user.email.lower().strip()
            user.adapt()
            user.validate()
            db.session.add(user)
            db.session.commit()
            self.log('[U] Imported user {0} {1} (type={2})'.format(
                     row['userID'],
                     user.full_name(),
                     user.type))
            count += 1
        result.close()
        self.log('[U] Imported {0} users'.format(count))

    def _import_tasks(self, connection):
        """
        Import tasks.
        """
        result = connection.execute(oldcrm.QUERY_TASKS)
        count = 0
        for row in result:
            if row['projectID'] is None:
                self.log('[T] Skipped task {0} because no project id'.
                         format(row['taskID']))
                continue
            if not row['title'] or len(row['title']) < 2:
                self.log('[T] Skipped task {0} because of bad title'.
                         format(row['taskID']))
                continue
            if row['taskID'] in (
                20756, 20757, 20758, 20776, 20777, 17751, 17752, 17753, 17754,
                17755, 17756, 17757, 17758, 17759, 17760, 17779, 17775, 17777,
                17780, 17782, 17783, 17784, 17785, 17786, 17787, 17788, 17790,
                17821
            ):
                self.log('[T] Skipped task {0} because is in bad tasks list'.
                         format(row['taskID']))
                continue
            task = Task()
            task.id = int(row['taskID'])
            task.project_id = row['projectID']
            task.name = self._format_text(row['title'].decode('cp1251'), remove_dot=True)
            task.text = row['taskText'].decode('cp1251')
            task.is_billable = row['billable']
            task.limit = row['durationLimit']
            task.status = TASK_STATUS_ASSIGNED
            if row['generalStatusID'] == 16:
                task.status = TASK_STATUS_CANCELLED
            if row['generalStatusID'] == 17:
                task.status = TASK_STATUS_COMPLETED
            if row['generalStatusID'] == 18:
                task.status = TASK_STATUS_FIXED
            if row['generalStatusID'] == 20:
                task.status = TASK_STATUS_REOPENED
            if row['generalStatusID'] == 46:
                task.status = TASK_STATUS_WAITFORCLIENT
            task.created = row['created']
            task.created_by_id = row['createdBy']
            if task.created_by_id == 429:  # Dmitry Semenov fix
                task.created_by_id = 103
            if task.created_by_id == 566:  # Richard Whiteman fix
                task.created_by_id = 106
            task.updated = row['updated']
            task.finished = row['finishDate']
            task.adapt()
            task.validate()
            db.session.add(task)
            db.session.commit()
            self.log(u'[T] Imported task {0} {1}'.format(task.id, task.name))
            count += 1
        result.close()
        self.log('[T] Imported {0} tasks'.format(count))
        return

    def _import_jobs(self, connection):
        """
        Import jobs.
        """
        result = connection.execute(oldcrm.QUERY_JOBS)
        count = 0
        for row in result:
            if len(row['description']) > 1024:
                self.log('[J] Skipped {0} because description too long'.
                         format(row['jobID']))
                continue
            job = Job()
            job.task_id = int(row['taskID'])
            if not db.session.query(exists().where(Task.id ==job.task_id)).scalar():
                self.log('[J] Skipped {0} because task {1} is missed'.
                         format(row['jobID'], str(job.task_id)))
                continue
            if row['jobID'] == 88402.0:
                pass
                #self.log('[J] Skipped job {0} because is in bad jobs list'.
                #         format(row['jobID']))
                #continue
            job.text = self._format_text(row['description'].decode('cp1251'), put_dot=True)
            job.is_billable = row['billable']
            job.duration = row['duration']
            job.date = row['jobDate']
            job.created_by_id = row['createdBy']
            if job.created_by_id == 429:  # Dmitry Semenov fix
                job.created_by_id = 103
            if job.created_by_id == 566:  # Richard Whiteman fix
                job.created_by_id = 106
            if job.date.year > 2014:
                self.log('[J] Skipped {0} because jobDate is incorrect'.
                         format(row['jobID']))
                continue
            job.adapt()
            job.validate(validate_duration=False)
            db.session.add(job)
            db.session.commit()
            count += 1
            if count % 5000 == 0:
                self.log('[J] Imported {0} jobs'.format(count))
            self.log('[J] Import {0} job id'.format(row['jobID']))
        result.close()
        self.log('[J] Imported {0} jobs'.format(count))

    def _import_clients(self, connection):
        """
        Import clients.
        """
        result = connection.execute(oldcrm.QUERY_CLIENTS)
        count = 0
        for row in result:
            client = Client()
            client.id = row['clientID']
            client.name = self._format_text(row['name'].decode('cp1251'), remove_dot=True)
            client.address1 = row['address1'].decode('cp1251')
            client.address2 = row['address2'].decode('cp1251')
            client.city = row['city']
            client.state = self._format_state(row['state'])
            client.zip = row['zip']
            client.country = self._format_country(row['country'])
            client.sale_user_id = row['sale_user_id']
            client.contract_till = row['contractTill']
            client.min_hours_per_month = row['minHoursPerMonth']
            client.phone = self._format_phone(row['phone'])
            client.fax = self._format_phone(row['fax'])
            client.email = self._format_phone(row['email'])
            client.website = row['website']
            client.billing_email = row['billing_email']
            client.billing_method = row['billableMethod']
            if client.billing_method and client.billing_method.strip() not in billing_methods:
                client.billing_method = CLIENT_BILLING_METHOD_UNKNOWN
            client.address1 = row['billing_address1'].decode('cp1251')
            client.address2 = row['billing_address2'].decode('cp1251')
            client.city = row['billing_city']
            client.state = self._format_state(row['billing_state'])
            client.zip = row['billing_zip']
            client.country = self._format_country(row['billing_country'])
            client.billing_notes = row['billableNote']
            client.created = row['created']
            client.updated = row['updated']
            client.removed = row['deleted']
            client.adapt()
            client.validate()
            db.session.add(client)
            db.session.commit()
            self.log(u'[C] Imported client {0} {1}'.format(client.id, client.name))
            count += 1
        self.log('[C] Imported {0} clients'.format(count))

    def _import_projects(self, connection):
        """
        Import projects.
        """
        result = connection.execute(oldcrm.QUERY_PROJECTS)
        count = 0
        for row in result:
            project = Project()
            project.id = row['projectID']
            project.name = self._format_text(row['title'], remove_dot=True)
            project.client_id = row['clientID']
            project.is_utilized = row['utilization']
            project.is_variable_billing_rate = row['billingVariable']
            project.is_visible_to_client = row['showClient']
            project.is_billable = row['billable']
            project.status = PROJECT_STATUS_ACTIVE
            if row['generalStatusID'] == 38:
                project.status = PROJECT_STATUS_MAINTENANCE
            if row['generalStatusID'] == 27:
                project.status = PROJECT_STATUS_COMPLETED
            if row['generalStatusID'] == 21:
                project.status = PROJECT_STATUS_CANCELLED
            project.start_date = row['startDate']
            project.finish_date = row['proposedFinishDate']
            if project.finish_date and project.start_date and project.finish_date < project.start_date:
                project.finish_date = None
            project.actual_finish_date = row['actualFinishDate']
            project.min_hours_per_month = row['minHoursPerMonth']
            project.limit = row['durationLimit']
            project.billing_type = row['billingType']
            project.billing_frequency = row['frequency']
            if project.billing_frequency not in billing_frequencies:
                project.billing_frequency = PROJECT_BILLING_FREQUENCY_UNKNOWN
            project.hosting_price_per_month = row['hostingFee']
            project.hosting_start_date = row['hostingStart']
            project.created = row['created']
            project.updated = row['updated']
            project.removed = row['deleted']
            project.adapt()
            project.validate()
            db.session.add(project)
            db.session.commit()
            self.log(u'[P] Imported project {0} {1}'.format(project.id, project.name))
            count += 1
        self.log('[P] Imported {0} projects'.format(count))
