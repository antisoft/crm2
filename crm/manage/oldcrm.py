# -*- coding: utf-8 -*-
"""
    crm.manage.oldcrm
    ~~~~~~~~~~~~~~~~~

    Old crm related data.
"""

QUERY_USERS = """
select
  users.userID,
  users.firstName,
  users.lastName,
  users.birthday,
  users.hideBirthday,
  users.typeID,
  isnull(authorizationData.[login], users.firstName + users.lastName) as 'login',
  isnull(authorizationData.[password], 'e6d4fa64f7c33440ee51bf6a48275d227841f24d') as 'password', -- ofj032Jasio21_421q3
  isnull(authorizationData.email,
    (select top 1 email from emails where typeID = 'P ' and targetID = users.documentID)) as 'email',
  (select phone from phones where targetID = users.documentID and typeID = 'MOB') as 'cell',
  (select phone from phones where targetID = users.documentID and typeID = 'TEL') as 'phone',
  (select phone from phones where targetID = users.documentID and typeID = 'SKP') as 'skype',
  documents.deleted,
  documents.created,
  documents.updated,
  documents.generalStatusID
from users
left join authorizationData on users.userID = authorizationData.userID
inner join documents on users.documentID = documents.documentID
where users.typeID = 'E ' or users.typeID = 'C '
"""

QUERY_JOBS = """
select
  jobs.jobID,
  jobs.jobDate,
  jobs.createdBy,
  jobs.billable,
  jobs.duration,
  jobs.[description],
  isnull(jobs.projectID, documents.projectID) as 'projectID',
  isnull(documents.objectID,
    isnull((select top 1 taskid from tasks where projectid = jobs.projectID and
      title in ('misc', 'misc2', 'Product Management', 'Provide Bookkeeping Services', 'Development',
        'Project Management, General', 'Organizational, Misc', 'Office Management', 'self education',
        'Human Resource Management | CA', 'Client Meetings, Preparing Quotes, General Sales Work',
        'Self Education | KZ', 'MW: Project Management', 'Human Resource Management | KZ',
        'Project Management', 'AvidTrip Design Project', 'deck 62')), 18423)) as 'taskID'
from jobs
left join documents on jobs.targetID = documents.documentID and documents.typeID = 'T ';
"""

QUERY_CLIENTS = """
select
  clients.clientID,
  clients.name,
  isnull([address].address, '') as 'address1',
  isnull([address].address2, '') as 'address2',
  isnull([address].city, '') as 'city',
  isnull([address].state, '') as 'state',
  isnull([address].zip, '') as 'zip',
  isnull([address].country, '') as 'country',
  (select top 1 employeeUserID from client_managers where clientID = clients.clientID and roleID = 'SM ')
    as 'sale_user_id',
  contractTill,
  minHoursPerMonth,
  (select top 1 phone from phones where targetID = clients.documentID and typeID = 'TEL') as 'phone',
  (select top 1 phone from phones where targetID = clients.documentID and typeID = 'FAX') as 'fax',
  (select top 1 email from emails where targetID = clients.documentID and typeID = 'P  ') as 'email',
  website,
  (select top 1 email from emails where targetID = clients.documentID and typeID = 'B  ') as 'billing_email',
  isnull(billableMethod, 'U') as 'billableMethod',
  isnull(billing_address.address, '') as 'billing_address1',
  isnull(billing_address.address2, '') as 'billing_address2',
  isnull(billing_address.city, '') as 'billing_city',
  isnull(billing_address.state, '') as 'billing_state',
  isnull(billing_address.zip, '') as 'billing_zip',
  isnull(billing_address.country, '') as 'billing_country',
  isnull(billableNote, '') as 'billableNote',
  documents.created,
  documents.updated,
  documents.deleted
from
  clients
  inner join documents on clients.documentID = documents.documentID
  outer apply (select top 1 * from addresses where targetID = clients.documentID and typeID = 'P ') [address]
  outer apply (select top 1 * from addresses where targetID = clients.documentID and typeID = 'B ') billing_address
"""

QUERY_PROJECTS = """
select
  projects.projectID,
  title,
  projects.clientID,
  utilization,
  billingVariable,
  showClient,
  billable,
  documents.generalStatusID,
  startDate,
  proposedFinishDate,
  actualFinishDate,
  minHoursPerMonth,
  durationLimit,
  isnull(billingType, 'U') as 'billingType',
  isnull(Frequency, 'U') as 'frequency',
  hostingFee,
  hostingStart,
  created,
  updated,
  deleted
from
  projects
  inner join documents on projects.documentID = documents.documentID
"""

QUERY_TASKS = """
select
  taskID,
  title,
  taskText,
  billable,
  isnull(durationLimit, 0) as 'durationLimit',
  documents.generalStatusID,
  documents.created,
  documents.createdBy,
  documents.updated,
  finishDate,
  isnull(tasks.projectID, documents.projectID) as 'projectID',
  isnull(tasks.clientID, documents.clientID) as 'clientID'
from
  tasks
  inner join documents on tasks.documentID = documents.documentID
"""
