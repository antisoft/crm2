# -*- coding: utf-8 -*-
"""
    crm.core
    ~~~~~~~~

    Core module of application.
"""

from flask.ext.sqlalchemy import SQLAlchemy

#: Flask-SQLAlchemy db instance
db = SQLAlchemy()
