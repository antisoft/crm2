# -*- coding: utf-8 -*-
"""
    crm.models
    ~~~~~~~~~~

    Consolidate all models.
"""

from crm.domain.system.models import *
from crm.domain.clients.models import *
from crm.domain.jobs.models import *
from crm.domain.projects.models import *
from crm.domain.tasks.models import *
from crm.domain.users.models import *
