# -*- coding: utf-8 -*-
"""
    crm.domain
    ~~~~~~~~~~
"""

from sqlalchemy.dialects import sqlite

from crm.core import db


class ApplicationError(Exception):
    """Raises when application exception occurs"""
    def __init__(self, message='Application error', code=0):
        self.message = message
        self.code = code

    def __str__(self):
        return self.message


class ValidationError(ApplicationError):
    """Raises when validation exception occurs"""
    def __init__(self, message='Validation error', code=0, errors=None):
        self.message = message
        self.code = code
        self.errors = errors


class NotFoundError(ApplicationError):
    """Raises when object not found"""
    def __init__(self, message='Object not found', code=0):
        self.message = message
        self.code = code


class ForbiddenError(ApplicationError):
    """Raises when object not found"""
    def __init__(self, message='Forbidden ', code=0):
        self.message = message
        self.code = code


#: sqlite doesn't support bigint for auto increment
BigIntegerOrInteger = db.BigInteger().with_variant(sqlite.INTEGER(), 'sqlite')


class JsonSerializable(object):
    """
    A mixin that can be used to mark a SQLAlchemy model class which
    implements a :func:`to_json` method. The :func:`to_json` method is used
    in conjuction with the custom :class:`JSONEncoder` class. By default this
    mixin will assume all properties of the SQLAlchemy model are to be visible
    in the JSON output. Extend this class to customize which properties are
    public, hidden or modified before being being passed to the JSON serializer.
    """

    __json_public__ = None
    __json_hidden__ = None
    __json_modifiers__ = None

    def get_field_names(self):
        for p in self.__mapper__.iterate_properties:
            yield p.key

    def to_json(self):
        field_names = self.get_field_names()

        public = set(self.__json_public__ or field_names)
        hidden = set(self.__json_hidden__ or [])
        modifiers = self.__json_modifiers__ or dict()

        rv = dict()
        for key in public - hidden:
            rv[key] = getattr(self, key)
        for key, modifier in modifiers.items():
            value = getattr(self, key)
            rv[key] = modifier(value, self)
        return rv


class AppModel(JsonSerializable):
    def validate_before_clean(self):
        pass

    def clean(self):
        pass

    def validate_after_clean(self):
        pass

    def save(self, db):
        from sqlalchemy import inspect
        self.validate_before_clean()
        self.clean()
        self.validate_after_clean()
        if inspect(self).transient:
            db.session.add(self)
        db.session.commit()
