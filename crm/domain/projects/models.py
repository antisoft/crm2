# -*- coding: utf-8 -*-
"""
    crm.domain.projects.models
    ~~~~~~~~~~~~~~~~~~~~~~~~~~
"""

from datetime import datetime

from crm.core import db
from crm.domain import *
from crm.tools.validol import validate
from crm.tools.adapt import adapt


class Project(AppModel, db.Model):
    """
    Project model.
    """
    __tablename__ = 'projects'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    class Status(object):
        active = 1
        completed = 2
        maintenance = 3
        cancelled = 4
        all = {
            active: 'active',
            completed: 'completed',
            maintenance: 'maintenance',
            cancelled: 'cancelled'
        }

    class BillingType(object):
        unknown = 'u'
        fixed = 'f'
        hourly = 'h'
        investment = 'I'
        all = {
            unknown: 'unknown',
            fixed: 'fixed',
            hourly: 'hourly',
            investment: 'investment',
        }

    class BillingFrequency(object):
        unknown = 'u'
        weekly = 'w'
        monthly = 'm'
        bimonthly = 'b'
        quarterly = 'q'
        annual = 'a'
        scheduled = 's'
        all = {
            unknown: 'unknown',
            weekly: 'weekly',
            monthly: 'monthly',
            bimonthly: 'bimonthly',
            quarterly: 'quarterly',
            annual: 'annual',
            scheduled: 'scheduled',
        }

    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=True, nullable=False)
    name = db.Column(db.Unicode(255), nullable=False)
    client_id = db.Column(BigIntegerOrInteger, db.ForeignKey('clients.id'), nullable=False)
    is_utilized = db.Column(db.Boolean(), nullable=False, default=True)
    is_variable_billing_rate = db.Column(db.Boolean(), nullable=False, default=False)
    is_visible_to_client = db.Column(db.Boolean(), nullable=False, default=True)
    is_billable = db.Column(db.Boolean(), nullable=False, default=True)
    status = db.Column(db.Integer(), nullable=False)
    start_date = db.Column(db.DateTime())
    finish_date = db.Column(db.DateTime())
    actual_finish_date = db.Column(db.DateTime())
    min_hours_per_month = db.Column(db.Integer(), nullable=False, default=0)
    max_hours_per_month = db.Column(db.Integer(), nullable=False, default=0)
    limit = db.Column(db.Integer(), nullable=False, default=0)
    mailing_list = db.Column(db.String(255), nullable=False, default='')
    billing_type = db.Column(db.CHAR(1), nullable=False)
    billing_frequency = db.Column(db.CHAR(1), nullable=False)
    hosting_price_per_month = db.Column(db.Integer, nullable=False, default=0)
    hosting_start_date = db.Column(db.DateTime())
    created = db.Column(db.DateTime(), nullable=False, default=datetime.now())
    updated = db.Column(db.DateTime())
    removed = db.Column(db.DateTime())

    def __init__(self, *args, **kwargs):
        super(Project, self).__init__(*args, **kwargs)
        self.is_billable = False
        self.is_utilized = True
        self.is_variable_billing_rate = False
        self.is_visible_to_client = True
        self.status = Project.Status.active
        self.min_hours_per_month = 0
        self.max_hours_per_month = 0
        self.limit = 0
        self.billing_type = Project.BillingType.unknown
        self.billing_frequency = Project.BillingFrequency.unknown
        self.hosting_price_per_month = 0
        self.created = datetime.now()

    def validate_after_clean(self):
        validate(self, 'id').integer().min(1)
        validate(self, 'name').len_range(1, 255).required()
        validate(self, 'is_utilized').bool().required()
        validate(self, 'is_variable_billing_rate').bool().required()
        validate(self, 'is_visible_to_client').bool().required()
        validate(self, 'is_billable').bool().required()
        validate(self, 'status').inside(Project.Status.all).required()
        if self.start_date and self.finish_date and self.finish_date < self.start_date:
            raise ValidationError('Project finish date cannot be greater than start date.')
        if (
            self.min_hours_per_month and self.max_hours_per_month and
            self.min_hours_per_month > self.max_hours_per_month
        ):
            raise ValidationError('Project min hours per month cannot be greater max hours per month.')
        if self.start_date and self.actual_finish_date and self.actual_finish_date < self.start_date:
            raise ValidationError('Project actual finish date cannot be greater than start date.')
        validate(self, 'min_hours_per_month').integer().min(0)
        validate(self, 'max_hours_per_month').integer().min(0)
        validate(self, 'billing_frequency').inside(Project.BillingFrequency.all).required()
        validate(self, 'billing_type').inside(Project.BillingType.all).required()
        validate(self, 'hosting_price_per_month').integer().min(0)

    def clean(self):
        self.name = (self.name or '').strip()
        self.mailing_list = (self.mailing_list or '').strip()
        self.hosting_price_per_month = self.hosting_price_per_month or 0
        self.min_hours_per_month = self.min_hours_per_month or 0
        self.max_hours_per_month = self.max_hours_per_month or 0
        self.billing_type = self.billing_type or Project.BillingType.unknown
        self.billing_frequency = self.billing_frequency or Project.BillingFrequency.unknown
        self.status = self.status or Project.Status.active
        self.limit = self.limit or 0
