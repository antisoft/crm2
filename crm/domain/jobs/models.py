# -*- coding: utf-8 -*-
"""
    crm.domain.jobs.models
    ~~~~~~~~~~~~~~~~~~~~~~

    Jobs models.
"""

from datetime import datetime
from sqlalchemy import exists

from crm.core import db
from crm.tools.validol import validate
from crm.domain import *

from crm.domain.users.models import User
from crm.domain.tasks.models import Task


class Job(db.Model, AppModel):
    """
    Job model.
    """
    __tablename__ = 'jobs'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=True,
                   nullable=False)
    text = db.Column(db.Unicode(1024), nullable=False)
    is_billable = db.Column(db.Boolean, nullable=False, default=False)
    duration = db.Column(db.Integer(), nullable=False, default=0)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.now())
    task_id = db.Column(BigIntegerOrInteger, db.ForeignKey('tasks.id'),
                        nullable=False)
    created_by_id = db.Column(BigIntegerOrInteger, db.ForeignKey('users.id'),
                              nullable=False)

    def validate_after_clean(self, validate_duration=True):
        validate(self, 'id').integer().min(1)
        validate(self, 'text').len_range(1, 1024).required()
        if validate_duration:
            validate(self, 'duration').required().integer(). \
                when(self.id > 0).min(0). \
                otherwise().min(1)
        validate(self, 'date').required().date()
        validate(self, 'created_by_id').required().min(1)
        validate(self, 'task_id').required().min(1)
        validate(self.date, 'year', name='date year').range(2006, datetime.now().year+1)

    def validate_deps(self):
        if not db.session.query(exists().where(Task.id == self.task_id)).scalar():
            raise NotFoundError('Invalid task id')
        if not db.session.query(exists().where(User.id == self.created_by_id)).scalar():
            raise NotFoundError('Invalid created by user id')

    def clean(self):
        self.text = (self.text or '').strip()
