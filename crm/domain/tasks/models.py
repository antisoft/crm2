# -*- coding: utf-8 -*-
"""
    crm.domain.tasks.models
    ~~~~~~~~~~~~~~~~~~~~~~~

    Tasks models.
"""

from datetime import datetime

from crm.core import db
from crm.domain import *
from crm.tools.validol import validate


class Task(db.Model, AppModel):
    """
    Task model.
    """
    __tablename__ = 'tasks'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    class Status(object):
        assigned = 1
        fixed = 2
        reopened = 3
        cancelled = 4
        completed = 5
        waitforclient = 6
        all = {
            assigned: 'assigned',
            fixed: 'fixed',
            reopened: 'reopened',
            cancelled: 'cancelled',
            completed: 'completed',
            waitforclient: 'waitforclient',
        }

    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=True,
                   nullable=False)
    name = db.Column(db.String(255), nullable=False)
    text = db.Column(db.UnicodeText(), nullable=False, default=u'')
    is_billable = db.Column(db.Boolean(), nullable=False, default=False)
    limit = db.Column(db.Integer(), nullable=False, default=0)
    status = db.Column(db.Integer(), nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.now())
    created_by_id = db.Column(BigIntegerOrInteger,
                              db.ForeignKey('users.id'), nullable=False)
    updated = db.Column(db.DateTime())
    finished = db.Column(db.DateTime())
    tested = db.Column(db.DateTime())
    completed = db.Column(db.DateTime())
    reopened = db.Column(db.DateTime())
    project_id = db.Column(BigIntegerOrInteger,
                           db.ForeignKey('projects.id'), nullable=False)

    def __init__(self, *args, **kwargs):
        super(Task, self).__init__(*args, **kwargs)
        self.is_billable = False
        self.limit = 0
        self.status = Task.Status.assigned
        self.created = datetime.now()

    def validate_after_clean(self):
        validate(self, 'id').integer().min(1)
        validate(self, 'name').len_range(1, 255).required()
        validate(self, 'limit').min(0).required()
        validate(self, 'status').required().inside(Task.Status.all)
        validate(self, 'created_by_id').required()
        validate(self, 'project_id').required()

    def clean(self):
        self.name = (self.name or '').strip()
