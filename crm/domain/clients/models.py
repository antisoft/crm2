# -*- coding: utf-8 -*-
"""
    crm.domain.clients.models
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    Client related items.
"""

from datetime import datetime

from crm.core import db
from crm.tools.validol import validate
from crm.tools.adapt import adapt
from crm.domain import *
from crm.domain.users.models import STATES, COUNTRIES


class Client(db.Model, AppModel):
    """
    Client model.
    """
    __tablename__ = 'clients'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    class BillingMethod(object):
        unknown = 'u'
        email = 'e'
        mail = 'm'
        both = 'b'

        all = {
            unknown: 'unknown',
            both: 'both',
            email: 'email',
            mail: 'mail',
        }

    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=True,
                   nullable=False)
    name = db.Column(db.String(255), nullable=False)
    address1 = db.Column(db.String(255), nullable=False, default='')
    address2 = db.Column(db.String(255), nullable=False, default='')
    city = db.Column(db.String(255), nullable=False, default='')
    zip = db.Column(db.String(10), nullable=False, default='')
    state = db.Column(db.String(5), nullable=False, default='CA')
    country = db.Column(db.String(5), nullable=False, default='USA')
    sale_user_id = db.Column(db.BigInteger(), db.ForeignKey('users.id'))
    contract_signed = db.Column(db.DateTime())
    contract_till = db.Column(db.DateTime())
    min_hours_per_month = db.Column(db.Integer(), nullable=False, default=0)
    phone = db.Column(db.String(40))
    fax = db.Column(db.String(40))
    email = db.Column(db.String(255))
    website = db.Column(db.String(255))
    billing_email = db.Column(db.String(255))
    billing_method = db.Column(db.CHAR(1),
                               default=BillingMethod.unknown)
    billing_address1 = db.Column(db.String(255), nullable=False, default='')
    billing_address2 = db.Column(db.String(255), nullable=False, default='')
    billing_city = db.Column(db.String(255), nullable=False, default='')
    billing_zip = db.Column(db.String(10), nullable=False, default='')
    billing_state = db.Column(db.String(5), nullable=False, default='CA')
    billing_country = db.Column(db.String(5), nullable=False, default='USA')
    billing_notes = db.Column(db.UnicodeText())
    created = db.Column(db.DateTime(), nullable=False, default=datetime.now())
    updated = db.Column(db.DateTime())
    removed = db.Column(db.DateTime())

    def __init__(self, *args, **kwargs):
        super(Client, self).__init__(*args, **kwargs)
        self.state = 'CA'
        self.country = 'USA'
        self.min_hours_per_month = 0
        self.billing_state = 'CA'
        self.billing_country = 'USA'
        self.billing_method = Client.BillingMethod.unknown
        self.created = datetime.now()

    def validate_after_clean(self):
        validate(self, 'id').min(1).integer()
        validate(self, 'name').len_range(1, 255).required()
        validate(self, 'address1').len_range(0, 255)
        validate(self, 'address2').len_range(0, 255)
        validate(self, 'city').len_range(0, 255)
        validate(self, 'zip').when(len(self.zip) > 0).validate().zip()
        validate(self, 'state').when(len(self.state) > 0).validate(). \
            inside(STATES)
        validate(self, 'country').when(len(self.country) > 0). \
            validate().inside(COUNTRIES)
        validate(self, 'phone').len_range(0, 255)
        validate(self, 'fax').len_range(0, 255)
        validate(self, 'email').len_range(0, 255). \
            when(len(self.email) > 0).email()
        validate(self, 'website').len_range(0, 255)

        validate(self, 'billing_method').inside(Client.BillingMethod.all)
        validate(self, 'billing_address1').len_range(0, 255)
        validate(self, 'billing_address2').len_range(0, 255)
        validate(self, 'billing_city').len_range(0, 255)
        validate(self, 'billing_zip'). \
            when(len(self.billing_zip) > 0).validate().zip()
        validate(self, 'billing_state'). \
            when(len(self.billing_state) > 0).validate().inside(STATES)
        validate(self, 'billing_country'). \
            when(len(self.billing_country) > 0).validate().inside(COUNTRIES)

        if (self.contract_signed and self.contract_till and
            self.contract_signed < self.contract_till):
            raise ValidationError('Contract signed date cannot be less than\
                                  contract till date.')

    def clean(self):
        self.name = (self.name or '').strip()
        self.address1 = (self.address1 or '').strip()
        self.address2 = (self.address2 or '').strip()
        self.city = (self.city or '').strip()
        self.zip = (self.zip or '').strip()
        self.state = (self.state or '').strip()
        self.country = (self.country or '').strip()
        self.email = (self.email or '').strip()
        self.min_hours_per_month = self.min_hours_per_month or 0
        self.billing_address1 = (self.billing_address1 or '').strip()
        self.billing_address2 = (self.billing_address2 or '').strip()
        self.billing_city = (self.billing_city or '').strip()
        self.billing_zip = (self.billing_zip or '').strip()
        self.billing_state = (self.billing_state or '').strip()
        self.billing_country = (self.billing_country or '').strip()
        self.billing_email = (self.billing_email or '').strip()
        self.website = (self.website or '').strip().lower()
