# -*- coding: utf-8 -*-
"""
    crm.domain.users.models
    ~~~~~~~~~~~~~~~~~~~~~~~

    User related models.
"""

from datetime import datetime

from crm.core import db
from crm.tools.validol import validate
from crm.tools.adapt import adapt
from crm.domain import *


class UserRole(db.Model, AppModel):
    """
    User roles.
    """
    __tablename__ = 'user_roles'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    
    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=True,
                   nullable=False)
    user_id = db.Column(BigIntegerOrInteger,
                        db.ForeignKey('users.id'), nullable=False)
    role = db.Column(db.String(30), nullable=False)

    def validate_after_clean(self):
        validate(self, 'role').inside(User.Role.all).required()

    def clean(self):
        self.role = (self.role or '').lower().strip()


class User(db.Model, AppModel):
    """
    User model.
    """
    __tablename__ = 'users'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    __json_hidden__ = ['roles']

    class Status(object):
        active = 1
        disabled = 2
        fired = 3
        all = {
            active: 'active',
            disabled: 'disabled',
            fired: 'fired',
        }

    class Role(object):
        admin = 'admin'
        account_manager = 'acm'
        developer = 'dev'
        sales_manager = 'sales'
        tester = 'tester'
        project_manager = 'project'
        human_resource_manager = 'hr'
        design = 'design'
        technical_manager = 'tech'
        guest = 'guest'
        client = 'client'
        all = {
            admin: 'system admin',
            account_manager: 'account manager',
            developer: 'developer',
            sales_manager: 'sales manager',
            tester: 'tester',
            project_manager: 'project manager',
            human_resource_manager: 'human resource manager',
            design: 'design',
            technical_manager: 'technical manager',
            guest: 'guest',
            client: 'client'
        }

    class Type(object):
        employee = 'e'
        system = 's'
        client = 'c'
        all = {
            employee: 'employee',
            system: 'system',
            client: 'client',
        }

    class Gender(object):
        unknown = 'u'
        male = 'm'
        female = 'f'
        all = {
            unknown: 'unknown',
            male: 'male',
            female: 'female',
        }

    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=True, nullable=False)
    type = db.Column(db.CHAR(1), nullable=False)
    first_name = db.Column(db.Unicode(255), nullable=False)
    last_name = db.Column(db.Unicode(255), nullable=False)
    login = db.Column(db.String(40), nullable=False)
    email = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(100), nullable=False, default='')
    last_login = db.Column(db.DateTime())
    status = db.Column(db.Integer(), nullable=False)
    gender = db.Column(db.CHAR(1), nullable=False)
    phone = db.Column(db.String(40), nullable=False, default='')
    cell = db.Column(db.String(40), nullable=False, default='')
    skype = db.Column(db.String(255), nullable=False, default='')
    birthday = db.Column(db.DateTime())
    created = db.Column(db.DateTime(), nullable=False, default=datetime.now())
    updated = db.Column(db.DateTime())
    removed = db.Column(db.DateTime())
    roles = db.relationship(UserRole, backref='user', lazy='select')

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        self.type = User.Type.employee
        self.status = User.Status.active
        self.gender = User.Gender.unknown

    ### Flask-Login specific

    def is_active(self):
        return self.status == User.Status.active and self.removed is None

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def validate_after_clean(self):
        validate(self, 'id').min(1).integer()
        validate(self, 'first_name').len_range(1, 255).required()
        validate(self, 'last_name').len_range(1, 255).required()
        validate(self, 'login').len_range(1, 40).required()
        validate(self, 'email').email()
        validate(self, 'password').len_range(5, 255)
        validate(self, 'last_login').datetime()
        validate(self, 'status').inside(User.Status.all).required()
        validate(self, 'phone').len_range(0, 40)
        validate(self, 'cell').len_range(0, 40)
        validate(self, 'skype').len_range(0, 40)
        validate(self, 'birthday').date()

    def clean(self):
        self.id = int(self.id)
        self.first_name = (self.first_name or '').strip()
        self.last_name = (self.last_name or '').strip()
        self.status = self.status or User.Status.active
        self.email = (self.email or '').strip().lower()
        self.login = (self.login or '').strip().lower()
        self.password = (self.password or '').strip().lower()
        self.skype = (self.skype or '').strip().lower()

    def full_name(self):
        return u'{0} {1}'.format(self.first_name, self.last_name).encode('ascii', 'ignore')

    def set_password(self, password):
        from hashlib import sha1
        self.password = sha1(password).hexdigest().lower()

    def has_role(self, role):
        list = [x.role for x in self.roles]
        return role in list

    def add_role(self, role):
        if self.has_role(role):
            return True
        user_role = UserRole()
        user_role.user_id = self.id
        user_role.role = role
        user_role.clean()
        user_role.validate_after_clean()
        self.roles.append(user_role)
        return True

    def remove_role(self, role):
        user_role = next((x for x in self.roles if x.role == role), None)
        if user_role is not None:
            self.roles.remove(user_role)


class Employee(db.Model, AppModel):
    """
    Employee model.
    """
    __tablename__ = 'employees'
    __table_args__ = {'mysql_engine': 'InnoDB'}
 
    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=False,
                   nullable=False)
    branch_id = db.Column(db.SmallInteger(), db.ForeignKey('branches.id'),
                          nullable=False)
    department_id = db.Column(db.Integer(), db.ForeignKey('departments.id'),
                              nullable=False)
    alternative_emails = db.Column(db.String(255), nullable=False, default='')
    is_utilized = db.Column(db.Boolean(), nullable=False, default=True)
    is_part_time = db.Column(db.Boolean(), nullable=False, default=False)
    is_daily_report_required = db.Column(db.Boolean(), nullable=False,
                                         default=False)
    is_onsite = db.Column(db.Boolean(), nullable=False, default=True)
    address1 = db.Column(db.String(255), nullable=False, default='')
    address2 = db.Column(db.String(255), nullable=False, default='')
    city = db.Column(db.String(255), nullable=False, default='')
    zip = db.Column(db.String(10), nullable=False, default='')
    state = db.Column(db.String(5), nullable=False, default='CA')
    country = db.Column(db.String(5), nullable=False, default='USA')


class Department(db.Model, AppModel):
    """
    Department model.
    """
    __tablename__ = 'departments'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True,
                   nullable=False)
    name = db.Column(db.String(255), nullable=False)
    is_utilized = db.Column(db.Boolean(), nullable=False, default=True)

    def validate_after_clean(self):
        validate(self.id).min(1).integer()
        validate(self.name).range_len(1, 255).required()
        validate(self.is_utilized).bool()

    def clean(self):
        self.name = (self.name or '').strip()


class Branch(db.Model, AppModel):
    """
    Branch model.
    """
    __tablename__ = 'branches'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = db.Column(db.SmallInteger(), primary_key=True, autoincrement=True,
                   nullable=False)
    name = db.Column(db.String(255), nullable=False)

    def validate_after_clean(self):
        validate(self, 'id').min(1).integer()
        validate(self, 'name').len_range(1, 255).required()

    def clean(self):
        self.name = (self.name or '').strip()

#: states list
STATES = {
    'AK': 'Alaska',
    'AL': 'Alabama',
    'AR': 'Arkansas',
    'AS': 'American Samoa',
    'AZ': 'Arizona',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DC': 'District of Columbia',
    'DE': 'Delaware',
    'FL': 'Florida',
    'GA': 'Georgia',
    'GU': 'Guam',
    'HI': 'Hawaii',
    'IA': 'Iowa',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'MA': 'Massachusetts',
    'MD': 'Maryland',
    'ME': 'Maine',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MO': 'Missouri',
    'MP': 'Northern Mariana Islands',
    'MS': 'Mississippi',
    'MT': 'Montana',
    'NA': 'National',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'NE': 'Nebraska',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NV': 'Nevada',
    'NY': 'New York',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'PR': 'Puerto Rico',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VA': 'Virginia',
    'VI': 'Virgin Islands',
    'VT': 'Vermont',
    'WA': 'Washington',
    'WI': 'Wisconsin',
    'WV': 'West Virginia',
    'WY': 'Wyoming',
}

#: countries list
COUNTRIES = {
    'RUS': 'Russia',
    'USA': 'USA',
    'KAZ': 'Kazakhstan',
    'VNM': 'Vietnam',
    'GBR': 'United Kingdom',
    'CAN': 'Canada',
}
