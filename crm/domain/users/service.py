# -*- coding: utf-8 -*-
"""
    crm.domain.users.service
    ~~~~~~~~~~~~~~~~~~~~~~~~
"""

from hashlib import sha1

from crm.core import db
from models import *
from crm.domain import *
from crm.helpers import *


def authenticate(login, password):
    """
    Checks email and password and returns user object.

    @rtype: crm.domain.users.models.User
    """
    if not login or not login.strip():
        raise ValidationError('No login provided')
    password = password or ''

    login = login.lower().strip()
    user = User.query.filter(User.login==login).first()
    if not user:
        raise ValidationError('Login or password incorrect')
    if not user.password and not password:
        return user
    if user.password != password:
        raise ValidationError('Login or password incorrect')
    return user


def change_password(user, old_password, new_password):
    """
    Change user's password.

    @rtype: crm.domain.users.models.User
    """
    if user.password and user.password != sha1(old_password).hexdigest().lower():
        raise ValidationError('Old password doesn\'t match')
    user.set_password(new_password)
