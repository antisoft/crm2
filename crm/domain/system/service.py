# -*- coding: utf-8 -*-
"""
    crm.domain.system.service
    ~~~~~~~~~~~~~~~~~~~~~~~~~
"""

from models import *
from crm.core import db


def log_info(text, user_id=None, **kwargs):
    log = Log(text=text, created_by_id=user_id, type=Log.Type.info)
    log.save(db)


def log_error(text, user_id=None, **kwargs):
    log = Log(text=text, created_by_id=user_id, type=Log.Type.error)
    log.save(db)
