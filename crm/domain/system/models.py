# -*- coding: utf-8 -*-
"""
    crm.domain.system.models
    ~~~~~~~~~~~~~~~~~~~~~~~~
"""

from datetime import datetime

from crm.core import db
from crm.tools.validol import validate
from crm.tools.adapt import adapt
from crm.domain import *


class Log(db.Model, AppModel):
    """
    Log application entry.
    """
    __tablename__ = 'logs'

    class Type(object):
        trace = 't'
        debug = 'd'
        info = 'i'
        warn = 'w'
        error = 'e'
        fatal = 'f'
        all = {
            trace: 'trace',
            debug: 'debug',
            info: 'info',
            warn: 'warning',
            error: 'error',
            fatal: 'fatal',
        }

    class EntityType(object):
        unknown = 0
        system = 1
        misc = 2
        service = 3
        login = 4
        client = 100
        project = 101
        task = 102
        job = 103
        user = 104

    id = db.Column(BigIntegerOrInteger, primary_key=True, autoincrement=True,
                   nullable=False)
    type = db.Column(db.CHAR(1), nullable=False)
    text = db.Column(db.UnicodeText(), nullable=False, default=u'')
    created_by_id = db.Column(BigIntegerOrInteger, db.ForeignKey('users.id'))
    created = db.Column(db.DateTime(), nullable=False, default=datetime.now())
    misc_entity1 = db.Column(db.Integer, nullable=False)
    misc_value1 = db.Column(db.Unicode(255), default=u'')
    misc_entity2 = db.Column(db.Integer, nullable=False)
    misc_value2 = db.Column(db.Unicode(255), default=u'')
    misc_entity3 = db.Column(db.Integer, nullable=False)
    misc_value3 = db.Column(db.Unicode(255), default=u'')
    misc_entity4 = db.Column(db.Integer, nullable=False)
    misc_value4 = db.Column(db.Unicode(255), default=u'')

    def validate_after_clean(self):
        validate(self, 'id').integer().min(1)
        validate(self, 'type').inside(Log.Type.all.keys()).required()
        validate(self, 'text').required()

    def clean(self):
        self.text = (self.text or '').strip()
        self.misc_value1 = self.misc_value1 or ''
        self.misc_value2 = self.misc_value2 or ''
        self.misc_value3 = self.misc_value3 or ''
        self.misc_value4 = self.misc_value4 or ''
        self.misc_entity1 = self.misc_entity1 or Log.EntityType.unknown
        self.misc_entity2 = self.misc_entity2 or Log.EntityType.unknown
        self.misc_entity3 = self.misc_entity3 or Log.EntityType.unknown
        self.misc_entity4 = self.misc_entity4 or Log.EntityType.unknown
