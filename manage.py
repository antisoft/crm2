#!/usr/bin/env python
"""
    manage
    ~~~~~~

    Manage module. Is used for import and initial database creation
"""

from flask.ext.script import Manager
from crm.api import create_app
from crm.manage.database import CreateDatabaseCommand

manager = Manager(create_app())
manager.add_command('create_database', CreateDatabaseCommand())

if __name__ == '__main__':
    manager.run()
