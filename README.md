Welcome to CRM
==============

CRM is Saritasa system that allows to organize our business processes.

Project setup
=============

Here are steps you need to setup project for development. Project uses Python 2.7 and Flask as web framework. MySQL is used for data sore. Required Linux packages

- libmysqlclient-dev (for mysql_config);
- python;
- python-virtualenv;
- tdsodbc
- python-pyodbc

Create virtual environment

    $ mkdir crm2 | cd crm2
    $ virtualenv .
    $ source bin/activate

It refers to python dist-package pyodbc that doesn't work in virtual env by default. I solved it by adding following link to bin/activate:

    export PYTHONPATH=$PYTHONPATH:/usr/lib/python2.7/dist-packages

Probably not the best solution but it works :). Install necessary python packages. All source code must be place to ./crm2/source folder.

    $ pip install -r  source/requirement.txt

Create database from scratch or you better migrate from old one

    $ ./manage.py create_database --migrate mssql://sa:123@crmdev
    $ cat /etc/odbc.ini

```
[ODBC Data Sources]
crmdev       = MSSQL

[crmdev]
Driver       = FreeTDS
Description  = ODBC connection via FreeTDS
SERVER       = 192.168.111.30
PORT         = 1433
USER         = sa
Password     = 123
Database     = CRM.Production
OPTION       = 3
SOCKET       =
```
