# -*- coding: utf-8 -*-
"""
    tests.user_tests
    ~~~~~~~~~~~~~~~~
"""

from . import AppTestCase
from crm.domain.users.models import *
from crm.core import db
from data import *


class UserTest(AppTestCase):
    def test_user_add(self):
        prepare_environment_1()
        user = get_user_tm_ivan()
        headers = self.login(user.login, user.password)

        response = self.client.post('users/', data={
            'first_name': 'Ivan',
            'last_name': 'Kozhin',
            'login': 'kozhin',
            'email': 'ivan@saritasa.com',
            'password': '12345',
            'gender': 'M',
            'phone': '+79082146138',
            'cell': '+79082146138',
            'skype': 'ivan.kras',
        }, headers=headers)
        response = self.client.get('users/' + str(response.json['data']['id']),
                                   headers=headers)

        assert response.json['success']
        db.session.commit()

    def test_user_update(self):
        prepare_environment_1()
        user = get_user_tm_ivan()
        headers = self.login(user.login, user.password)

        response = self.client.put('users/' + str(user.id), data={
            'first_name': 'Ivan',
            'last_name': 'Kozhin',
            'login': 'kozhin',
            'email': 'ivan@saritasa.com',
            'password': '12345',
            'gender': 'M',
            'phone': '911',
            'cell': '911',
            'skype': 'ivan.kras',
        }, headers=headers)

        response = self.client.get('users/' + str(response.json['data']['id']),
                                   headers=headers)

        assert response.json['success']
        assert response.json['data']['cell'] == u'911'
        db.session.commit()

    def test_user_roles(self):
        user = get_user_tm_ivan()

        user.add_role(User.Role.guest)
        user.add_role(User.Role.admin)
        assert user.has_role(User.Role.admin)

        user.add_role(User.Role.developer)
        user.add_role(User.Role.client)
        user.remove_role(User.Role.guest)
        assert user.has_role(User.Role.developer)
        assert not user.has_role(User.Role.guest)
