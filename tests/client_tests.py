# -*- coding: utf-8 -*-
"""
    tests.client_tests
    ~~~~~~~~~~~~~~~~~~
"""

from . import AppTestCase
from crm.models import *
from crm.core import db
from data import *


class ClientTest(AppTestCase):
    def test_client_add(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        response = self.client.post('clients/', data={
            'name': 'antiSoft',
            'website': 'http://www.anti-soft.ru/en',
            'email': 'fwd2ivan@gmail.com',
        }, headers=headers)
        response = self.client.get('clients/' +
                                   str(response.json['data']['id']),
                                   headers=headers)
        assert response.json['success']

    def test_client_update(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        client_id = get_client_formi9().id
        response = self.client.put('clients/' + str(client_id), data={
            'name': 'test name',
            'fax': '(234) 124-2142'
        }, headers=headers)

        db.session.expunge_all()
        response = self.client.get('clients/' + str(client_id),
                                   headers=headers)

        assert response.json['success']
        assert response.json['data']['fax'] == '(234) 124-2142'

    def test_client_remove(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        client_id = get_client_formi9().id
        response = self.client.delete('clients/' + str(client_id),
                                      headers=headers)
        assert response.json['success']

    def test_client_search(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        response = self.client.get('clients/', query_string={
            'name': 'form',
        }, headers=headers)

        assert response.json['total'] == 1
