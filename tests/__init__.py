# -*- coding: utf-8 -*-
"""
    crm.tests
    ~~~~~~~~~
"""

from flask.ext.testing import TestCase

from crm.api import create_app
from crm.core import db
from data import *


class AppTestCase(TestCase):
    def create_app(self):
        app = create_app()
        app.config['TESTING'] = True
        app.config['DEBUG'] = True
        app.config['SECRET_KEY'] = 'this_is_the_development_secret_key'
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        #app.config['SQLALCHEMY_ECHO'] = True
        return app

    def setUp(self):
        super(AppTestCase, self).setUp()
        db.create_all()

    def tearDown(self):
        super(AppTestCase, self).tearDown()
        db.drop_all()

    def login(self, login, password):
        response = self.client.post('auth/token', data=dict(
            login=login,
            password=password
        ))
        headers = [
            ('Authorization', response.json['data']['token']),
        ]
        return headers
