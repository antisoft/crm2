# -*- coding: utf-8 -*-
"""
    tests.project_tests
    ~~~~~~~~~~~~~~~~~~~
"""

from . import AppTestCase
from crm.domain.users.models import *
from crm.core import db
from data import *


class ProjectTest(AppTestCase):
    def test_project_add(self):
        prepare_environment_1()
        user = get_user_tm_ivan()
        headers = self.login(user.login, user.password)

        response = self.client.post('projects/', data={
            'name': 'My new super fast CRM',
            'client_id': get_client_formi9().id,
        }, headers=headers)
        print response.json

        response = self.client.get('projects/' + str(response.json['data']['id']),
                                   headers=headers)
        assert response.json['success']
        db.session.commit()

    def test_project_update(self):
        prepare_environment_1()
        user = get_user_tm_ivan()
        headers = self.login(user.login, user.password)

        project = get_project_formi9()
        response = self.client.put('projects/' + str(project.id), data={
            'name': 'Form I-10',
            'client_id': get_client_formi9().id,
        }, headers=headers)

        response = self.client.get('projects/' + str(response.json['data']['id']),
                                   headers=headers)

        assert response.json['success']
        assert response.json['data']['name'] == u'Form I-10'
        db.session.commit()
