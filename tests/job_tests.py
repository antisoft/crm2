# -*- coding: utf-8 -*-
"""
    tests.job_tests
    ~~~~~~~~~~~~~~~
"""

from . import AppTestCase
from crm.models import *
from crm.core import db
from data import *


class JobTest(AppTestCase):
    def test_job_add(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        response = self.client.post('jobs/', data={
            'text': 'test job',
            'is_billable': True,
            'duration': 120,
            'date': '2014-03-03',
            'task_id': get_task_for_crm().id,
        }, headers=headers)

        response = self.client.get('jobs/' + str(response.json['data']['id']),
                                   headers=headers)

        assert response.json['success']

    def test_job_update(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        response = self.client.post('jobs/', data={
            'text': 'test job',
            'is_billable': True,
            'duration': 120,
            'date': '2014-03-03',
            'task_id': get_task_for_crm().id,
        }, headers=headers)

        job_id = response.json['data']['id']
        response = self.client.put('jobs/' + str(job_id), data={
            'text': 'test job',
            'is_billable': True,
            'duration': 60,  # changed this
            'date': '2014-03-03',
            'task_id': get_task_for_crm().id,
        }, headers=headers)

        db.session.expunge_all()
        response = self.client.get('jobs/' + str(job_id),
                                   headers=headers)
        assert response.json['success']
        assert response.json['data']['duration'] == 60

    def test_job_remove(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        response = self.client.post('jobs/', data={
            'text': 'test job',
            'is_billable': True,
            'duration': 120,
            'date': '2014-03-03',
            'task_id': get_task_for_crm().id,
        }, headers=headers)

        job_id = response.json['data']['id']
        response = self.client.delete('jobs/' + str(job_id), headers=headers)
        print response.json

        assert response.json['data']['duration'] == 0

    def test_job_search(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        job = Job()
        job.text = u'test job'
        job.duration = 120
        job.date = datetime(2014, 3, 3)
        job.is_billable = False
        job.task_id = get_task_for_crm().id
        job.created_by_id = get_user_pm_dmitry().id
        job.clean()
        db.session.add(job)
        db.session.commit()

        response = self.client.get('jobs/', query_string={
            #'text': 'test',
            'is_billable': False,
            'date_from': '2014-03-01',
            'date_to': '2014-03-05',
            'task_id': get_task_for_crm().id,
        }, headers=headers)

        assert response.json['total'] == 1
