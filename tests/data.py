# -*- coding: utf-8 -*-
"""
    tests.data
    ~~~~~~~~~~
"""

from crm.models import *
from crm.core import db


def prepare_environment_1():
    db.session.add(get_user_tm_ivan())
    db.session.add(get_user_tester_alena())
    db.session.add(get_user_pm_dmitry())
    db.session.add(get_user_sales_mike())

    db.session.add(get_client_formi9())
    db.session.add(get_client_inhouse())

    db.session.add(get_project_crm())
    db.session.add(get_project_formi9())
    db.session.add(get_project_misc())

    db.session.add(get_task_for_crm())
    db.session.commit()


### users

def get_user_tm_ivan():
    user = User(id=1, first_name=u'Ivan', last_name=u'Kozhin', email=u'ivan@saritasa.com',
        login=u'kozhin', password='40bd001563085fc35165329ea1ff5c5ecbdbbeef')
    user.add_role(User.Role.admin)
    return user


def get_user_pm_dmitry():
    user = User(id=2, first_name=u'Dmitry', last_name=u'Semenov', email=u'dmitry@saritasa.com',
        login=u'semenov', password='40bd001563085fc35165329ea1ff5c5ecbdbbeef')
    return user


def get_user_tester_alena():
    user = User(id=3, first_name=u'Alena', last_name=u'Kolmogorova',
        email=u'kolmogorova@interesnee.ru', login=u'kolmogorova',
        password='40bd001563085fc35165329ea1ff5c5ecbdbbeef')
    return user


def get_user_sales_mike():
    user = User(id=4, first_name=u'Mike', last_name=u'Studley', email=u'mike@saritasa.com',
        login=u'mike', password='40bd001563085fc35165329ea1ff5c5ecbdbbeef')
    return user


### projects

def get_project_crm():
    project = Project(id=1, is_billable=False, name=u'CRM', is_utilized=True,
        status=Project.Status.active, client_id=get_client_inhouse().id)
    return project


def get_project_formi9():
    project = Project(id=2, is_billable=True, name=u'Form I-9', is_utilized=True,
        status=Project.Status.active, client_id=get_client_formi9().id)
    return project


def get_project_misc():
    project = Project(id=3, is_billable=False, name=u'Misc/Non-client', is_utilized=False,
        status=Project.Status.active, client_id=get_client_inhouse().id)
    return project


### clients

def get_client_inhouse():
    client = Client(id=1, name=u'In-house', email=u'nik@saritasa.com')
    return client


def get_client_formi9():
    client = Client(id=2, name=u'Form I-9 Auditing, LLC')
    return client


### tasks

def get_task_for_crm():
    task = Task(id=1, name=u'test task 1', text=u'test task text', limit=120,
        project_id=get_project_crm().id, created_by_id=get_user_pm_dmitry().id,
        status=Task.Status.assigned)
    return task


### jobs

def get_job_for_crm1():
    job = Job(id=1, text='test', created_by_id=get_user_pm_dmitry().id,
        task_id=get_task_for_crm().id, date='2014-04-04',is_billable = False, duration=60)
    return job
