# -*- coding: utf-8 -*-
"""
    tests.task_tests
    ~~~~~~~~~~~~~~~~
"""

from . import AppTestCase
from crm.models import *
from crm.core import db
from data import *


class TaskTest(AppTestCase):
    def test_task_add(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        response = self.client.post('tasks/', data={
            'name': 'Be original',
            'text': 'test job',
            'is_billable': True,
            'limit': 600,
            'status': Task.Status.assigned,
            'project_id': get_project_crm().id
        }, headers=headers)

        response = self.client.get('tasks/' + str(response.json['data']['id']),
                                   headers=headers)
        assert response.json['success']

    def test_task_remove(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        task_id = get_task_for_crm().id
        response = self.client.delete('tasks/' + str(task_id), headers=headers)

        assert response.json['data']['status'] == Task.Status.cancelled

    def test_task_update(self):
        prepare_environment_1()
        user = get_user_pm_dmitry()
        headers = self.login(user.login, user.password)

        task_id = get_task_for_crm().id
        response = self.client.put('tasks/' + str(task_id), data={
            'name': 'Be original',
            'text': 'test job',
            'is_billable': True,  # changed this
            'limit': 600,
            'status': Task.Status.assigned,
            'project_id': get_project_crm().id
        }, headers=headers)

        db.session.expunge_all()
        response = self.client.get('tasks/' + str(task_id),
                                   headers=headers)

        assert response.json['success']
        assert response.json['data']['is_billable'] is True
